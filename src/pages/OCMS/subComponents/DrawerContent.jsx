import React, { useState, useEffect } from "react";
import Clear from "@mui/icons-material/Clear";
import CloudUpload from "@mui/icons-material/CloudUpload";
import paymentImg from "../../../assets/payment.jpg";
import moment from "moment";

import {
  Button,
  TextField,
  Select,
  MenuItem,
  TextareaAutosize,
  Checkbox,
} from "@mui/material";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import Files from "react-butterfiles";
import axios from "axios";

const renderHeader = (title, props) => {
  return (
    <div>
      <div className="flex justify-between items-center">
        <div className="font-bold">{title}</div>
        <Clear
          className="cursor-pointer"
          onClick={() => {
            props.setDrawerState(false);
          }}
        />
      </div>

      <div className="my-8 border-solid border-[0.5px] border-gray-200"></div>
    </div>
  );
};

const checkFIN = () => {
  if (FormData.identification) {
    console.log(FormData.identification.substring(0, 1));
  }
};

export default function DrawerContent(props) {
  const [MissingInfo, setMissingInfo] = useState(false);
  const [profileFormData, setProfileFormData] = useState({});
  const [FormData, setFormData] = useState({
    identification: null,
    checkboxAccurate: false,
    checkboxCommitOffence: false,
  });
  const [ShowExpired, setShowExpired] = useState(false);
  const [CurrentDrawerType, setCurrentDrawerType] = useState();
  const [LeaseStartDate, setLeaseStartDate] = useState(new Date());
  const [LeaseEndDate, setLeaseEndDate] = useState(new Date());
  const [UploadFiles, setUploadFiles] = useState();
  const [UploadFilesErrors, setUploadFilesErrors] = useState();
  const [Disable, setDisable] = useState(false);
  const [wrongStartDate, setWrongStartDate] = useState(false);
  const [wrongEndDate, setWrongEndDate] = useState(false);

  const ProfileHandleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setProfileFormData((values) => ({ ...values, [name]: value }));
  };

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setMissingInfo(false);

    if (name === "identification" && value === "G1234567L") {
      setShowExpired(true);
    } else {
      setShowExpired(false);
    }

    if (name === "postal") {
      if (value.length === 6) checkPostalCode(value);
    }
    if (name === "checkboxAccurate" || name === "checkboxCommitOffence") {
      setFormData((values) => ({
        ...values,
        [name]: event.target.checked,
      }));
    } else {
      setFormData((values) => ({ ...values, [name]: value }));
    }

    // console.log("FormData", FormData);
  };

  const updateUserProfile = async () => {
    // Update user in redux
    let currentUserCopy = JSON.parse(JSON.stringify(props.currentUser));

    profileFormData.contact
      ? (currentUserCopy.contact = profileFormData.contact)
      : (currentUserCopy.contact = currentUserCopy.contact =
          props.currentUser.contact);
    profileFormData.email
      ? (currentUserCopy.email = profileFormData.email)
      : (currentUserCopy.email = props.currentUser.email);

    props.updateReduxUser(currentUserCopy);

    //  call axios to append
    const result = await axios.patch(
      `${props.baseURL}/users/${props.currentUser._id}`,
      {
        contact: profileFormData.contact,
        email: profileFormData.email,
      }
    );
  };

  const checkPostalCode = async (postal) => {
    const url = `https://developers.onemap.sg/commonapi/search?searchVal=${postal}&returnGeom=Y&getAddrDetails=Y&pageNum=1`;

    // call onemap api to checl address using postal code
    const addressResult = (await axios.get(url)).data.results[0];

    console.log("addressResult", addressResult);
    setFormData((values) => ({ ...values, address: addressResult.ADDRESS }));
    console.log("FormData", FormData);
  };

  const createAssignment = async () => {
    let FormDataCopy = JSON.parse(JSON.stringify(FormData));
    const currentNoticeID = props.currentNoticeID;
    console.log("currentNoticeID", currentNoticeID);

    if (FormDataCopy.relationship === "Leased Vehicle") {
      FormDataCopy.leasedStartDate = LeaseStartDate;
      FormDataCopy.leasedEndDate = LeaseEndDate;
      FormDataCopy.othersReason = null;
    }

    if (FormDataCopy.relationship === "Others") {
      FormDataCopy.leasedStartDate = null;
      FormDataCopy.leasedEndDate = null;
    }

    // if (FormDataCopy.identification.substring(0, 1) === "S") {
    //   FormDataCopy.identificationType = "NRIC";
    // } else {
    //   FormDataCopy.identificationType = "FIN";
    // }

    FormDataCopy.noticeNo = currentNoticeID;

    // console.log("props.files", props.files);

    // FormDataCopy.files = props.files;
    // if (UploadFiles) {
    FormDataCopy.files = props.files[0];
    // } else {
    //   FormDataCopy.files = UploadFiles;
    // }

    console.log("FormDataCopy", FormDataCopy);

    const postResult = await axios.post(
      `${props.baseURL}/assignments`,
      FormDataCopy
    );

    // console.log("postResult", postResult);

    // console.log("currentNoticeID")

    // to change the noticeList so that in main OCMS will show pending
    let copyProp = JSON.parse(JSON.stringify(props));
    const newNoticeList = copyProp.noticesList.map((notice) => {
      if (notice.id === 4) {
        notice.status = "Pending URA Approval";
      }
      return notice;
    });

    console.log("newNoticeList", newNoticeList);

    props.setNoticesList(newNoticeList);

    setCurrentDrawerType("thankyouDrawer");
  };

  const validate = () => {
    console.log("FormData", FormData);
    if (
      !FormData.name ||
      !FormData.contact ||
      !FormData.email ||
      !FormData.postal ||
      !FormData.identification
    ) {
      setMissingInfo(true);
    } else {
      setDisable(true);
    }
  };

  // onMounted
  useEffect(async () => {
    setCurrentDrawerType(props.DrawerType);
  }, []);

  switch (CurrentDrawerType) {
    case "indvlDrawer":
    case "compDrawer":
      return (
        <div className="p-6">
          {renderHeader("EDIT MY PROFILE", props)}

          <div className="flex justify-between my-6">
            <div>NAME:</div>
            <div>{props.currentUser.name}</div>
          </div>

          <div className="flex justify-between  my-6">
            <div>{props.userType === "individual" ? "NRIC:" : "UEN:"}</div>
            <div>{props.currentUser["nric"]}</div>
          </div>

          <div className="flex justify-between items-center my-6">
            <div>CONTACT NO:</div>
            <TextField
              InputProps={{
                inputProps: {
                  style: { textAlign: "right" },
                },
              }}
              name="contact"
              variant="outlined"
              size="small"
              defaultValue={props.currentUser.contact}
              onChange={ProfileHandleChange}
            />
          </div>

          <div className="flex justify-between my-6">
            <div>EMAIL ADDRESS:</div>
            <TextField
              InputProps={{
                inputProps: {
                  style: { textAlign: "right" },
                },
              }}
              name="email"
              variant="outlined"
              size="small"
              defaultValue={props.currentUser.email}
              onChange={ProfileHandleChange}
            />
          </div>

          <Button
            fullWidth
            variant="outlined"
            onClick={() => {
              updateUserProfile();
              props.setDrawerState(false);
            }}
          >
            EDIT
          </Button>
        </div>
      );

    case "furnishDrawer":
      return (
        <div className="p-6">
          {!Disable &&
            renderHeader("SUBMIT PARTICULARS OF HIRER/DRIVER", props)}

          {Disable && renderHeader("SUMMARY", props)}

          <div className="flex justify-between my-6 items-center">
            <div>TYPE:</div>

            <Select
              disabled={Disable}
              size="small"
              name="identificationType"
              value={FormData.identificationType || 0}
              onChange={handleChange}
            >
              <MenuItem value={0}>Please Choose</MenuItem>
              <MenuItem value={"Individual"}>Individual</MenuItem>
              <MenuItem value={"Company"}>Company</MenuItem>
            </Select>
          </div>

          <div className="flex justify-between my-6 items-center">
            <div>
              {FormData.identificationType === "Individual"
                ? "** NRIC/FIN/Passport NUMBER:"
                : "** UEN:"}
            </div>

            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              name="identification"
              value={FormData.identification || ""}
              onChange={handleChange}
              placeholder={
                FormData.identificationType === "Individual"
                  ? "NRIC/FIN/Passport NUMBER"
                  : "UEN"
              }
            />
          </div>

          {ShowExpired && (
            <div className="flex justify-between  my-6 items-center text-red-600 font-bold">
              <div>The Workpass has expired</div>
            </div>
          )}

          <div className="flex justify-between  my-6 items-center">
            <div>** NAME:</div>
            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              placeholder="NAME"
              name="name"
              value={FormData.name || ""}
              onChange={handleChange}
            />
          </div>

          <div className="flex justify-between  my-6 items-center">
            <div>** CONTACT NUMBER:</div>
            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              placeholder="CONTACT NUMBER"
              name="contact"
              value={FormData.contact || ""}
              onChange={handleChange}
            />
          </div>

          <div className="flex justify-between  my-6 items-center">
            <div>** EMAIL:</div>

            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              placeholder="EMAIL"
              name="email"
              value={FormData.email || ""}
              onChange={handleChange}
            />
          </div>

          <div className="flex justify-between my-6 items-center">
            <div>** POSTAL CODE:</div>

            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              placeholder="POSTAL CODE"
              name="postal"
              value={FormData.postal || ""}
              onChange={handleChange}
            />
          </div>

          <div className="flex justify-between my-6 items-center">
            <div>ADDRESS:</div>

            <TextField
              disabled={Disable}
              variant="outlined"
              size="small"
              placeholder="ADDRESS"
              name="address"
              value={FormData.address || ""}
              onChange={handleChange}
            />
          </div>

          <div className="flex justify-between my-6 items-center">
            <div>RELATION WITH OWNER:</div>

            <Select
              disabled={Disable}
              label="Relationship"
              labelId="demo-simple-select-helper-label"
              size="small"
              name="relationship"
              value={FormData.relationship || 0}
              onChange={handleChange}
            >
              <MenuItem value={0}>Please Choose</MenuItem>
              <MenuItem value={"Family"}>Family</MenuItem>
              <MenuItem value={"Leased Vehicle"}>Leased Vehicle</MenuItem>
              <MenuItem value={"Employee"}>Employee</MenuItem>
              <MenuItem value={"Others"}>Others</MenuItem>
            </Select>
          </div>

          {FormData.relationship === "Leased Vehicle" && (
            <div>
              <div className="flex justify-between my-6 items-center">
                <div>LEASED DATE FROM:</div>

                <div>
                  <DatePicker
                    disabled={Disable}
                    className="p-2"
                    selected={LeaseStartDate}
                    onFocus={() => setWrongStartDate(false)}
                    onChange={(date) => {
                      const currentNotice = props.noticesList.find(
                        (list) => list.noticeNo === props.currentNoticeID
                      );
                      const currentOffenseDate = currentNotice.offenseDate;

                      if (moment(currentOffenseDate).diff(moment(date)) < 0) {
                        // show the message
                        setWrongStartDate(true);
                      } else {
                        setWrongStartDate(false);
                        setLeaseStartDate(date);
                      }
                    }}
                  />
                </div>
              </div>
              {wrongStartDate && (
                <div className="my-6 items-center text-red-600">
                  The Start Date must be earlier than the Offense Date
                </div>
              )}

              <div className="flex justify-between my-6 items-center">
                <div>LEASED DATE TO:</div>

                <div>
                  <DatePicker
                    disabled={Disable}
                    className="p-2"
                    selected={LeaseEndDate}
                    onFocus={() => setWrongEndDate(false)}
                    onChange={(date) => {
                    
                      const currentNotice = props.noticesList.find(
                        (list) => list.noticeNo === props.currentNoticeID
                      );
                      const currentOffenseDate = currentNotice.offenseDate;

                      if (moment(currentOffenseDate).diff(moment(date)) > 0) {
                        // show the message
                        setWrongEndDate(true);
                      } else {
                        setWrongEndDate(false);
                        setLeaseEndDate(date);
                      }
                    }}
                  />
                </div>
              </div>

              {wrongEndDate && (
                <div className="my-6 items-center text-red-600">
                  The End Date must be later than the Offense Date
                </div>
              )}
            </div>
          )}

          {FormData.relationship === "Others" && (
            <div className="flex justify-between my-6 items-center">
              <div>OTHERS:</div>

              <div className="border-2 rounded-md p-3">
                <TextareaAutosize
                  disabled={Disable}
                  aria-label="minimum height"
                  minRows={3}
                  placeholder="OTHERS"
                  style={{ width: 200 }}
                  name="othersReason"
                  onChange={handleChange}
                />
              </div>
            </div>
          )}

          <Files
            convertToBase64
            multiple={false}
            maxSize="2mb"
            multipleMaxSize="10mb"
            accept={["application/pdf", "image/jpg", "image/jpeg"]}
            onSuccess={(files) => {
              console.log("files", files);
              setUploadFiles(files[0]);
              props.uploadFiles(files[0]);
            }}
            onError={(errors) => setsetUploadFilesError({ errors })}
          >
            {({ browseFiles }) => (
              <div className="flex justify-between">
                <div className="flex gap-x-2">
                  <div>ATTACH SUPPORTING FILES</div>
                  {!Disable && (
                    <CloudUpload
                      className="cursor-pointer"
                      onClick={browseFiles}
                    />
                  )}
                </div>

                {UploadFiles && (
                  // UploadFiles.files.map((file, index) => (
                  //   <div key={index} className="flex flex-col items-end">
                  //     {file.type !== "application/pdf" && (
                  //       <img
                  //         className="w-20"
                  //         key={file.name}
                  //         src={file.src.base64}
                  //       ></img>
                  //     )}
                  //     <div className="text-xs">{file.name}</div>
                  //   </div>
                  // ))

                  <div className="flex">
                    <div className="flex flex-col items-end">
                      {UploadFiles.type !== "application/pdf" && (
                        <img
                          className="w-20"
                          key={UploadFiles.name}
                          src={UploadFiles.src.base64}
                        ></img>
                      )}
                      {/* <div className="text-xs">{UploadFiles.name}</div> */}
                    </div>
                    <Clear
                      className="cursor-pointer"
                      onClick={() => {
                        setUploadFiles(null);
                      }}
                    />
                  </div>
                )}
              </div>
            )}
          </Files>

          {!Disable && (
            <div className="flex justify-between my-6 items-center">
              <Checkbox name="checkboxCommitOffence" onChange={handleChange} />
              <div>
                Did the furnished individual commit the offence or was in charge
                of the vehicle?
              </div>
            </div>
          )}

          {Disable && (
            <div className="flex justify-between my-6 items-center">
              <Checkbox name="checkboxAccurate" onChange={handleChange} />

              <div>I declare that the information submitted is accurate.</div>
            </div>
          )}

          {MissingInfo && (
            <div className="flex justify-between  my-6 items-center text-red-600">
              <div>
                ** Missing Mandatory Information or Checkbox not selected
              </div>
            </div>
          )}

          {!Disable && (
            <Button
              className="mt-6"
              fullWidth
              disabled={!FormData.checkboxCommitOffence}
              variant="outlined"
              onClick={() => {
                // setDisable(true);
                validate();
                // // props.setDrawerState(false);
                // console.log(CurrentDrawerType);
                // setCurrentDrawerType("summaryDrawer");
                // // currentDrawerType = "thankyouDrawer"
                // console.log(CurrentDrawerType);
              }}
            >
              PROCEED
            </Button>
          )}

          {Disable && (
            <div className="mt-10 flex justify-center gap-x-4">
              <Button
                variant="outlined"
                onClick={() => {
                  // setCurrentDrawerType("furnishDrawer");
                  setDisable(false);
                }}
              >
                BACK
              </Button>
              <Button
                variant="outlined"
                disabled={!FormData.checkboxAccurate}
                onClick={() => {
                  // setCurrentDrawerType("thankyouDrawer");
                  createAssignment();
                }}
              >
                SUBMIT
              </Button>
            </div>
          )}
        </div>
      );

    case "thankyouDrawer":
      return (
        <div className="p-6">
          {renderHeader("SUBMIT PARTICULARS OF HIRER/DRIVER", props)}

          <div className="flex justify-between  my-6 items-center text-red-600 font-bold">
            <div>Thank you. URA has received your submission.</div>
          </div>
        </div>
      );

    case "paymentDrawer":
      return (
        <div className="p-6">
          {renderHeader("PAYMENT", props)}

          <div className="flex justify-center">
            <img src={paymentImg} />
          </div>
        </div>
      );

    default:
      return null;
  }
}
