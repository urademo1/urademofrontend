import React from "react";
import CreditCard from "@mui/icons-material/CreditCard";
import CreateNewFolderIcon from "@mui/icons-material/CreateNewFolder";

export default function BottomRow(props) {
  console.log("BottomRow props", props);
  if (props.status != "Pending URA Approval") {
    return (
      <div className="mx-4 my-2 flex gap-x-12 text-gray-500 text-sm items-center justify-end text-xs">
        <div
          className="flex items-center gap-x-1 cursor-pointer"
          onClick={() => {
            props.setDrawerType("paymentDrawer");
            props.setDrawerState(true);
          }}
        >
          <CreditCard />
          <div className="font-bold">PAY</div>
        </div>

        <div
          className="flex items-center gap-x-1 cursor-pointer"
          onClick={() => {
            props.setDrawerType("furnishDrawer");
            props.setDrawerState(true);
            props.setCurrentNoticeID(props.currentNoticeID);
          }}
        >
          <CreateNewFolderIcon />

          <div className="uppercase font-bold">
            Furnish the particulars of the hirer/driver
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="mx-4 my-2 flex gap-x-12 text-gray-500 items-center justify-end text-xs">
        <div className="flex gap-x-2 font-bold">
          <div>STATUS:</div>
          <div>Pending URA Approval</div>
        </div>
      </div>
    );
  }
}
