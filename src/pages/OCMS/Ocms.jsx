import React, { useState, useEffect } from "react";
import singpassImg from "../../assets/welcome_banner.svg";
import moment from "moment";

// import subcomponents
import BottomRow from "./subComponents/BoomRow";
import DrawerContent from "./subComponents/DrawerContent";

import { Drawer, Button } from "@mui/material";
import PictureAsPdfOutlined from "@mui/icons-material/PictureAsPdfOutlined";
import { ThemeProvider } from "@mui/material/styles";
import { buttonTheme } from "../../components/theme";

// redux
import { useSelector, useDispatch } from "react-redux";
import { setUser } from "../../store/slices/globalSlice";
import axios from "axios";

const Ocms = () => {
  const [DrawerState, setDrawerState] = useState(false);
  const [DrawerType, setDrawerType] = useState("indvlDrawer");
  const [noticesList, setNoticesList] = useState([]);
  const [currentNoticeID, setCurrentNoticeID] = useState();
  const [files, setFiles] = useState([]);

  const dispatch = useDispatch();
  const drawerWidth = 440;

  // redux
  const baseURL = useSelector((state) => state.global.baseURL);
  const currentUser = useSelector((state) => state.global.user);
  const currentNoticesList = useSelector((state) => state.global.notices);

  const updateReduxUser = (user) => {
    console.log("user", user);
    dispatch(setUser(user));
  };

  const uploadFiles = async (file) => {
    let formData = new FormData();
    formData.append("file", file.src.file);

    const config = {
      headers: { "content-type": "multipart/form-data" },
    };

    const uploadResult = await axios.post(
      `${baseURL}/uploadimages`,
      formData,
      config
    );

    // console.log("uploadResult", uploadResult);

    const imageURL = `${baseURL}/uploads/${uploadResult.data.filename}`;

    setFiles([imageURL]);

    // console.log("imageURL", imageURL);
  };

  // onMounted
  useEffect(async () => {
    console.log("currentNoticesList", currentNoticesList)
    setNoticesList(currentNoticesList);

    // // get notices from API
    // const result = await axios.get(
    //   `${baseURL}/notices?userID=${currentUser._id}&$sort[id]=1`
    // );

    // // update the notieList state
    // setNoticesList(result.data.data);
  }, []);

  return (
    <div className="p-6 bg-white">
      <div className="md:flex md:justify-between">
        <div>
          <div className="font-bold text-md">Welcome,</div>

          <div className="flex items-center gap-5">
            <div className="font-bold">{currentUser.name}</div>
            <ThemeProvider theme={buttonTheme}>
              <Button
                className="text-slate-200"
                variant="outlined"
                onClick={() => {
                  if (currentUser.userType === "individual") {
                    setDrawerType("indvlDrawer");
                  } else {
                    setDrawerType("compDrawer");
                  }

                  setDrawerState(true);
                }}
              >
                EDIT
              </Button>
            </ThemeProvider>
          </div>

          <div className="text-sm">
            {currentUser.userType === "individual" ? "NRIC" : "UEN"}:{" "}
            {currentUser["nric"]}
          </div>
          <div className="text-sm">CONTACT NO: {currentUser.contact}</div>
          <div className="text-sm">EMAIL ADDRESS: {currentUser.email}</div>

          <div className="text-xs mt-2">
            Last login on {moment().subtract(7, "days").format("DD MMM YYYY")}{" "}
            at {moment().format("h:mm:ss a")}
          </div>
        </div>

        <div>
          <img className="mt-6 md:mt-2" src={singpassImg} />
        </div>
      </div>

      <div className="mx-2 p-6 bg-red-600 rounded-md text-white flex justify-between items-center text-lg">
        <div className="flex flex-col items-center grow">
          <div className="font-bold">Your Current Notices</div>
          <div className="text-sm">as at {moment().format("DD MMM YYYY")}</div>
        </div>
        <PictureAsPdfOutlined sx={{ fontSize: 40 }} />
      </div>

      {/* {currentUser._id === "620db61cd4dea007106e398c" && ( */}
      {currentUser._id === 34 && (
        <div className="p-4 text-lg text-red-600">
          <div>No outstanding URA furnishable notices.</div>
        </div>
      )}

      <div>
        {noticesList.map((notice) => {
          return (
            <div key={notice.id}>
              <div className="my-4 p-4 md:grid md:grid-cols-5 text-gray-500 text-sm">
                <div className="col-span-2">
                  <div>
                    {moment(notice.offenseDate).format("DD MMM YYYY h:mma")}
                  </div>
                  <div className="font-bold text-lg text-black uppercase">
                    Offence: {notice.offense}
                  </div>
                  <div>Carpark: {notice.carpark}</div>
                  <div>Location: {notice.location}</div>
                </div>
                <div className="md:flex md:flex-col md:items-center">
                  <div>Notice No: {notice.noticeNo}</div>
                  <div>Vehicle No: {notice.vehicleNo}</div>
                </div>

                {notice.personFurnished ? (
                  <div>
                    <div>Person Furnished: {notice.personFurnished}</div>
                    <div>Date Furnished: {notice.dateFurnished}</div>
                  </div>
                ) : (
                  <div></div>
                )}

                <div>
                  <div className="flex justify-end text-lg text-red-600">
                    Amount: ${notice.amount}
                  </div>
                  <div className="flex justify-end">
                    Due Date: {moment(notice.dueDate).format("DD MMM YYYY")}
                  </div>
                </div>
              </div>

              <div className="mx-4 border-solid border-[0.5px] border-gray-300" />
              {console.log("currentNoticeID", currentNoticeID)}
              <BottomRow
                status={notice.status}
                _id={notice.id}
                setCurrentNoticeID={setCurrentNoticeID}
                setDrawerState={setDrawerState}
                setDrawerType={setDrawerType}
                currentNoticeID={notice.noticeNo}
              />
              <div className="mx-4 border-dashed border-[0.5px] border-red-600" />
            </div>
          );
        })}
      </div>

      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        anchor="right"
        open={DrawerState}
        onClose={() => setDrawerState(false)}
      >
        <DrawerContent
          DrawerType={DrawerType}
          setDrawerState={setDrawerState}
          currentUser={currentUser}
          userType={currentUser.userType}
          baseURL={baseURL}
          setUser={setUser}
          // setCurrentUser={setCurrentUser}
          updateReduxUser={updateReduxUser}
          currentNoticeID={currentNoticeID}
          uploadFiles={uploadFiles}
          files={files}
          setNoticesList={setNoticesList}
          noticesList={noticesList}
        />
      </Drawer>
    </div>
  );
};

export default Ocms;
