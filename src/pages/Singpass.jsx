import React from "react";
import singpassImg from "../assets/singpass.png";
import { useNavigate } from "react-router";
import axios from "axios";

// redux
import { useDispatch, useSelector } from "react-redux";
import {
  setUser,
  setLoginStatus,
  setNotices,
} from "../store/slices/globalSlice";

// const baseURL = "http://localhost:3030/users";

const Singpass = () => {
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const baseURL = useSelector((state) => state.global.baseURL);

  const getUserFromDB = async (_id) => {
    return await axios.get(`${baseURL}/users/${_id}`).catch((error) => {
      console.log(error);
    });
  };

  const getUNoticeData = async (_id) => {
    return await axios.get(`${baseURL}/notices?userId=${_id}`);
  };

  const getUser = async (_id) => {
    const resultData = await Promise.all([
      getUserFromDB(_id),
      getUNoticeData(_id),
    ]);
    console.log("userData", resultData[0]);
    console.log("noticeData", resultData[1]);

    if (!resultData[1].data) {
      resultData[1].data = [];
    }

    dispatch(setLoginStatus(true));

    //  sequelize uses id, mongoose uses _id
    resultData[0].data._id = resultData[0].data.id;

    dispatch(setUser(resultData[0].data));

    dispatch(setNotices(resultData[1].data));

    navigate("/ocms");
  };

  return (
    <div className="bg-gray-200 p-3">
      <div className="flex justify-center my-2">
        <img
          src={singpassImg}
          className="object-cover object-center h-[430px] cursor-pointer"
          onClick={(e) => {
            // to use company CTL + Mouse Click
            // Individual, mouse click
            if (e.ctrlKey) {
              // getUser("62032d0508811258dc435b72");
              getUser("33");
            } else if (e.shiftKey) {
              // getUser("620db61cd4dea007106e398c");
              getUser("34");
            } else {
              // getUser("62032d0f08811258dc435b74");
              getUser("32");
            }
          }}
        />
      </div>
      <div className="flex justify-end mx-4 text-xs text-red-800 font-bold">
        For individual user with no notices, SHIFT + Click
      </div>
      <div className="flex justify-end mx-4 text-xs text-red-800 font-bold">
        For company user, CTL + Click
      </div>
    </div>
  );
};

export default Singpass;
