import React from "react";
import bannerImg from "../assets/AboutUs-Panorama1.jpg";

const Home = () => {
  return (
    <div className="relative">
      <img
        src={bannerImg}
        className="object-cover object-center w-full h-[430px] brightness-[0.70]"
      />

      <div className="absolute text-white text-3xl bottom-10 left-10 shadow-lg">
        <div>Make Singapore a great city</div>
        <div className="font-bold">to live, work and pray</div>
      </div>
    </div>
  );
};

export default Home;
