import React from "react";
import ReactDOM from "react-dom";
import store from "./store";
import { Provider } from "react-redux";
import "./index.css";

import { BrowserRouter, Routes, Route } from "react-router-dom";

// layout
import Layout from "./Layout/Layout";

// pages
import Home from "./pages/Home";
import Singpass from "./pages/Singpass";
import Ocms from "./pages/OCMS/Ocms";
import About from "./pages/About";



// testing for login with no layout
import Login from "./pages/Login";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="" element={<Home />} />
            <Route path="singpass" element={<Singpass />} />
            <Route path="ocms" element={<Ocms />} />
            <Route path="about" element={<About />} />
          </Route>

          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
