import React from "react";

const Footer = () => {
  return (
    <div className="md:flex xl:mx-20 p-3 text-sm bg-gray-100 justify-between">
      <div className="gap-y-10 md:flex md:gap-3">
      <div>Report Vulnerability</div>
      <div>Privacy Statement</div>
      <div>Terms of Use</div>
      <div>Rate This Site</div>
      <div>Site Requirements</div>
      <div>Sitemap</div>
      </div>
     
      <div>©2022 Urban Redevelopment Authority</div>
    </div>
  );
};

export default Footer;
