import React from "react";
import lionMark from "./../assets/lion-mark.svg";
import uraLogo from "./../assets/URALogo.png";
import Button from "@mui/material/Button";
import Dehaze from "@mui/icons-material/Dehaze";
import Notifications from "@mui/icons-material/Notifications";
import { buttonTheme } from "./theme";
import { ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router";


// redux
import { useDispatch, useSelector } from "react-redux";
import { setUser, setLoginStatus, setNotices } from "../store/slices/globalSlice";

const Header = () => {
  const dispatch = useDispatch();
  const loginStatus = useSelector((state) => state.global.value);

  let navigate = useNavigate();

  const butClicked = () => {
    navigate("/singpass");
  };

 

  const logoutClicked = () => {
    dispatch(setLoginStatus(false));
    dispatch(setUser({}));
    dispatch(setNotices([]));

    navigate("/");
  };

  return (
    <ThemeProvider theme={buttonTheme}>
      <div>
        <div className="bg-gray-200 p-2 text-xs px-25">
          <div className="xl:mx-20 flex justify-center items-center gap-1">
            <img src={lionMark} className="h-3 w-3" />
            <p>A Singapore Government Agency Website</p>
          </div>
        </div>

        {/* End buttons */}
        <div className="p-3 xl:mx-20 flex justify-between items-center">
          <img src={uraLogo} className="h-14" />

          <div className="flex items-center gap-4">
            <Button
              variant="outlined"
              onClick={butClicked}
              className={loginStatus ? "invisible" : "visible"}
            >
              LOGIN
            </Button>

            <div
              className={
                loginStatus ? "visible flex gap-4 items-center" : "invisible"
              }
            >
              <Notifications color="primary" />
              <img
                src="https://pickaface.net/gallery/avatar/unr_random_180410_1905_z1exb.png"
                className="object-cover object-center w-8 h-8 rounded-full"
              />
            </div>

            <Dehaze color="primary" className="cursor-pointer" onClick={logoutClicked}/>
           
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

export default Header;
