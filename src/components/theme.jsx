import { createTheme } from "@mui/material/styles";
import { purple, grey, teal } from "@mui/material/colors";

const buttonTheme =  createTheme({
  palette: {
    primary: {
      main: grey[800],
    }
  },
});

export { buttonTheme };
