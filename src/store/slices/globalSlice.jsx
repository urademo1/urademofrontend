import { createSlice } from "@reduxjs/toolkit";

export const globalSlice = createSlice({
  name: "login",
  initialState: {
    value: false,
    user: {},
    notices : [],
    // baseURL: "http://128.199.104.45:3333",
    // baseURL: "http://localhost:8080",
    // baseURL: "https://urabackendapi.azurewebsites.net/"
    baseURL: "https://urademo-springboot-backend.azurewebsites.net/"
  },
  reducers: {
    setLoginStatus: (state, payload) => {
      state.value = payload.payload;
    },

    setUser: (state, payload) => {
      // console.log("payload", payload.payload)
      state.user = payload.payload;
    },

    setNotices: (state, payload) => {
      // console.log("payload", payload.payload)
      state.notices = payload.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  increment,
  decrement,
  incrementByAmount,
  setLoginStatus,
  setUser,
  setNotices
} = globalSlice.actions;

export default globalSlice.reducer;
