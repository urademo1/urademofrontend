import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './slices/globalSlice'

export default configureStore({
  reducer: {
    global: counterReducer,
  },
})