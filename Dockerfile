FROM node:16-alpine
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .

RUN npm install
RUN npm run build


#RUN rm -rf node_modules
#RUN rm -rf src

RUN npm install express

EXPOSE 8080

CMD [ "node", "server.js" ]