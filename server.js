const express = require("express");
const path = require("path");
const app = express();
const port = 8080;

// Static Middleware
app.use(express.static(path.join(__dirname, "dist")));

app.listen(port, function (error) {
  if (error) throw error;
  console.log(`Server created Successfully @${port}`);
});
